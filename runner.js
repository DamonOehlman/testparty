/* jshint node: true */
'use strict';

var async = require('async');
var cp = require('cartesian-product');
var fs = require('fs');
var path = require('path');
var mkdirp = require('mkdirp');
var uuid = require('uuid');
var matrix = require('./matrix');

/**
  ### testparty runner

  Test testparty runner module is used to validate, query and subsequently
  launch the tests contained in the test module across the cross section of
  browsers specified in the discovered .testpartyrc folder
**/
module.exports = function(opts) {
  var basePath = path.dirname(opts.config);
  var browsers = Object.keys(opts.browsers || {});

  function installModule(testModule, callback) {
    callback();
  }

  function prepScriptRunner(targetScript, callback) {
    var runnerPath = path.join(basePath, '_runners', uuid.v4());
    var packageData = {
      testling: {
        files: [ targetScript ]
      }
    };

    mkdirp(runnerPath, function(err) {
      if (err) {
        return callback(err);
      }

      fs.writeFile(
        path.join(runnerPath, 'package.json'),
        JSON.stringify(packageData, null, 2),
        'utf8',
        function(err) {
          callback(err, runnerPath);
        }
      );
    });
  }

  function runTest(testModule, callback, previousAttempt) {
    var testPath = path.join(basePath, 'node_modules', testModule);
    var scripts;

    try {
      // try and require the test module
      scripts = (require(testPath) || []).map(function(testScript) {
        // ensure we have an appropriate extension
        if (path.extname(testScript) === '') {
          testScript += '.js';
        }

        return path.join('..', '..', 'node_modules', testModule, testScript);
      });
    }
    catch (e) {
      // if the module could not be found, and there was no previous
      // attempt the install the module and go again
      if (e.code === 'MODULE_NOT_FOUND' && (! previousAttempt)) {
        return installModule(testModule, function(err) {
          if (err) {
            return callback(err);
          }

          // have another crack at running the test
          runTest(testModule, callback, true);
        });
      }

      return callback(e);
    }

    // prepare the runners
    async.map(scripts, prepScriptRunner, function(err, runnerPaths) {
      var testMatrix;

      if (err) {
        return callback(err);
      }

      // generate the test matrix, which is each browser combination
      // based on the number of input scripts
      testMatrix = cp.apply(null, scripts.map(function() {
        return browsers;
      }));

      // run the test matrix
      matrix.run(opts, testMatrix, runnerPaths, callback);
    });
  }

  return runTest;
};