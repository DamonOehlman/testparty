/* jshint node: true */
'use strict';

var debug = require('debug')('testparty-testling');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var path = require('path');
var formatter = require('formatter');
var testling = spawn.bind(
  spawn, path.resolve(__dirname, 'node_modules', '.bin', 'testling')
);
var autobrowse = require('autobrowse');

/**
  ## testling.js - Automation layer for testling

  This is a module that handles automation of
  [testling](https://github.com/substack/testling). This is required as
  while testling is a node module, it has been crafted specifically to work
  as a CLI and thus needs some child_process level helpers...
**/

/**
  ### monitor(args, callback)

  Monitor the testling process (args[0]) and launch the browser specified
  with the command (args[1]).
**/
exports.monitor = function(args, callback) {
  var psTestling = args[0];
  var psBrowser;
  var browserCommand = formatter(args[1])(process.env);

  // psTestling.stdout.on('data', function(data) {
  //   console.log(data.toString());
  // });

  debug('running browser command: ' + browserCommand);
  autobrowse(browserCommand, psTestling.url, function(err, automator) {
    if (err) {
      debug('unable to run browser: ' + browserCommand);
      return callback(err);
    }

    // TODO: set a timeout waiting

    // wait for testling to exit
    psTestling.on('exit', function(code) {
      debug('testling process finished with exitcode: ' + code);
      
      // terminate the automator
      automator.kill(function() {
        // trigger the callback
        callback(code !== 0 ? new Error('test suite failed') : null);
      });
    });
  });
};

/**
  ### start(script, callback)

  Start a testling child process.

**/
exports.start = function(runner, callback) {
  var ps = testling([runner, '-u']);

  function handleData(data) {
    // add the url to the process
    // TODO: check stuff
    ps.url = data.toString();

    // decouple listeners
    ps.stdout.removeListener('data', handleData);
    ps.removeListener('exit', handleExit);

    // trigger the callback
    callback(null, ps);
  }

  function handleExit(code, signal) {
    callback(new Error('testling exited with exit code: ' + code));
  }

  debug('started testling for runner: ' + runner);
  ps.stdout.on('data', handleData);
  ps.on('exit', handleExit);
};