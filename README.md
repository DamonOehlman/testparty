# testparty

Coordinated multi-browser, multi-participant test runner powered by
[testling](https://github.com/substack/testling).

__NOTE:__ Nothing to see at the moment, still building it.


[![NPM](https://nodei.co/npm/testparty.png)](https://nodei.co/npm/testparty/)


## Why?

If you are writing [libraries that build on WebRTC](http://www.rtc.io) (or
something similar) then you need to test interaction between the components
rather than just isolated unit tests.

It's time to hold a test PARTY!!!!

## How?

Running testparty (at this early stage) involves the creation of
"test packs".  A test pack is a self-contained project (or just a directory)
that contains a special `.testpartyrc` file.

This file specifies the browser matrix and the test modules that will be
run.  For example:

```json
{
  "browsers": {
    "Firefox Stable": "{{ BROWSERS_HOME }}/firefox/stable/firefox",
    "Firefox Beta": "{{ BROWSERS_HOME }}/firefox/beta/firefox"
  },

  "tests": [
    "rtc-testparty-channels"
  ]
}
```

At this stage, I'm experimenting with the idea of creating specific (and
reusable) test modules that will be either npm installed (or can be 
easily npm linked during development).

I am [undecided](https://github.com/DamonOehlman/testparty/issues/3) as to
whether this is the best way to go (it works comfortably at the moment),
but may hit limits in the future.

## matrix.js - test matrix execution

This module is used to consume a test matrix (a, b, ..., n) browser 
combinations against the specified scripts.  

### run(matrix, scripts, callback)

Run the matrix.

### testparty runner

Test testparty runner module is used to validate, query and subsequently
launch the tests contained in the test module across the cross section of
browsers specified in the discovered .testpartyrc folder

## testling.js - Automation layer for testling

This is a module that handles automation of
[testling](https://github.com/substack/testling). This is required as
while testling is a node module, it has been crafted specifically to work
as a CLI and thus needs some child_process level helpers...

### monitor(args, callback)

Monitor the testling process (args[0]) and launch the browser specified
with the command (args[1]).

### start(script, callback)

Start a testling child process.

## License(s)

### MIT

Copyright (c) 2013 Damon Oehlman <damon.oehlman@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
