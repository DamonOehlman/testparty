/* jshint node: true */
'use strict';

var async = require('async');
var runner = require('./runner');

/**
  # testparty

  Coordinated multi-browser, multi-participant test runner powered by
  [testling](https://github.com/substack/testling).

  __NOTE:__ Nothing to see at the moment, still building it.

  ## Why?

  If you are writing [libraries that build on WebRTC](http://www.rtc.io) (or
  something similar) then you need to test interaction between the components
  rather than just isolated unit tests.

  It's time to hold a test PARTY!!!!

  ## How?

  Running testparty (at this early stage) involves the creation of
  "test packs".  A test pack is a self-contained project (or just a directory)
  that contains a special `.testpartyrc` file.

  This file specifies the browser matrix and the test modules that will be
  run.  For example:

  ```json
  {
    "browsers": {
      "Firefox Stable": "{{ BROWSERS_HOME }}/firefox/stable/firefox",
      "Firefox Beta": "{{ BROWSERS_HOME }}/firefox/beta/firefox"
    },

    "tests": [
      "rtc-testparty-channels"
    ]
  }
  ```

  At this stage, I'm experimenting with the idea of creating specific (and
  reusable) test modules that will be either npm installed (or can be 
  easily npm linked during development).

  I am [undecided](https://github.com/DamonOehlman/testparty/issues/3) as to
  whether this is the best way to go (it works comfortably at the moment),
  but may hit limits in the future.

**/
module.exports = function(opts, callback) {
  // iterate through the tests and execute on the target platforms
  async.mapSeries(
    opts.tests || [],
    runner(opts),
    callback
  );
};