/* jshint node: true */
'use strict';

var debug = require('debug')('testparty');
var async = require('async');
var testling = require('./testling');
var pluck = require('whisk/pluck');
var zip = require('whisk/zip');

/**
  ## matrix.js - test matrix execution

  This module is used to consume a test matrix (a, b, ..., n) browser 
  combinations against the specified scripts.  

  ### run(matrix, scripts, callback)

  Run the matrix.

**/
var run = exports.run = function(opts, matrix, runners, callback) {
  var browsers;

  if (matrix.length === 0) {
    return callback();
  }

  // get the browsers to run off the matrix
  browsers = matrix.shift().map(function(browserName) {
    return opts.browsers[browserName];
  });

  // create a testling instance for each of the scripts
  async.map(runners, testling.start, function(err, processes) {
    if (err) {
      return callback(err);
    }

    async.map(
      processes.map(zip(browsers)),
      testling.monitor,

      // once done, continue with the next test
      function(err, results) {
        // if we encountered an error running the test then abort
        // the processing matrix
        if (err) {
          return callback(err);
        }

        // TODO: write the results

        // go again
        run(opts, matrix, runners, callback);
      }
    );
  });
};